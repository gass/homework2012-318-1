#include "exception.h"

using namespace std;
 
void DivideByZeroException::what() {
	cout<<"Fatal error: divide by zero\n";	
}

void OverflowSizeException::what() {
	cout<<"Error: Too long size. This version supports parameter <size> less than 1.000.000. Contact the developer for help.\n";
}
