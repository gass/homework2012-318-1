#include "Long.h"
#include "exception.h"

using namespace std;

int length(digit a) {
	int count;
	digit temp=a;
	if(temp==0)
		return 1;
	for(count=0; temp!=0; count++)
		temp=temp/10;
	
	return count;
}

int max(int a, int b) {
    if (a>b)
        return a;
    else
        return b;
}

Long::Long(const char * s) {
	int s_size=strlen(s);
	char *str=new char [s_size];
	strcpy(str,s);
	if (str[0]=='-') {
		sign=-1;
		str++;
		s_size--;
	}
	else {
		sign=1;
	}
	if (s_size%(length(BASE)-1)==0)
		size=s_size/(length(BASE)-1);
	else
		size=s_size/(length(BASE)-1)+1;

	if (size>MAXSIZE) {
		OverflowSizeException overflow_exception;
		throw overflow_exception;
	}
	number=new digit [size];
	for (int i=s_size, j=0; i>0 && j<size; i-=4, j++) {
		str[i] = 0;
		number[j]= atoi (i>=4 ? str+i-4 : str);
	}
}


Long::Long(const int num) {
	int temp=num, count;
	if (num < 0) {
		sign=-1;
		temp=temp*(-1);
	} else {
		sign=1;
	}
	if (temp<=BASE) {
		count=1;
	} else {
		for (count=1; (temp=temp/BASE)!=0; count++) {}
	}
	number = new digit  [size=count];
	if (sign<0)
		temp=num*(-1);
	else
		temp=num;
	for (count=0; count<size; count++) {
		number[count]=temp%BASE;
		temp=temp/BASE;
	}
}

Long::Long(const Long& l) {
	number = new digit [size=l.size];
	for (int i=0; i<size; i++)
		number[i]=l.number[i];
	sign = l.sign;
}

void Long::print()const { 
	if (sign<0)
		cout<<"- ";
	for (int i=size-1; i>=0; i--) {
		cout<<number[i]<<" ";
	}
	cout << "<" << size << ">";
}

Long::~Long() {
	delete []number;
}

Long abs(Long l) {
	l.sign=1;
	return l;
}

Long operator+(const Long& r, const Long& l) {
	Long tmp;
	if (r.sign==l.sign) {
		if ((tmp.size=max(l.size, r.size)+1)>MAXSIZE) {
			OverflowSizeException overflow_exception;
			throw overflow_exception;
		}
		tmp.number = new digit [tmp.size];
		memset(tmp.number, 0, sizeof(digit)*tmp.size);
		for (int i=0; i<tmp.size-1; i++) {
			if (i<l.size && i<r.size) {
				tmp.number[i]=l.number[i]+r.number[i];
			} else {
				if(tmp.size-1==l.size )
					tmp.number[i]=l.number[i];
				else
					tmp.number[i]=r.number[i];
			}
		}

		if (r.sign>0)
			tmp.sign=1;
		else
			tmp.sign=-1;
	} else {
		if (r.sign<0) {
			if (abs(r)<abs(l)) {
				tmp=abs(l)-abs(r);
				tmp.sign=1;
				return tmp;	
			} else {
				tmp=abs(r)-abs(l);
				tmp.sign=-1;
				return tmp;	
			}
		} else {
			if (abs(r)<abs(l)) {
				tmp=abs(l)-abs(r);
				tmp.sign=-1;	
				return tmp;
			} else {
				tmp=abs(r)-abs(l);
				tmp.sign=1;
				return tmp;	
			}
		}	

	}

	tmp.normalize_number();
	return tmp;
}

Long operator-( const Long& r, const Long& l) {
	Long tmp;
	if (r.sign==l.sign) {
		if (r==l) {
		    tmp=0;
		    return tmp;
	    }
		if (r.sign>0) {
			if (abs(r)>abs(l)) {
				if ((tmp.size=max(l.size, r.size))>MAXSIZE) {
					OverflowSizeException overflow_exception;
					throw overflow_exception;
				}
				tmp.number = new digit [tmp.size];
				for (int i=0; i<tmp.size; i++) {
					if (i<l.size && i<r.size) {
						tmp.number[i]=r.number[i]-l.number[i];
					} else {
						if (tmp.size==l.size )
							tmp.number[i]=l.number[i];
						else
							tmp.number[i]=r.number[i];
					}
				}
				tmp.sign=1;
				tmp.normalize_number();
				return tmp;
			} else {
				tmp=abs(l)-abs(r);
				tmp.sign=-1;
				return tmp;	
			}
			
		} else {
			if (abs(r)<abs(l)) {
				tmp=abs(l)-abs(r);
				tmp.sign=1;
				return tmp;	
			} else {
				tmp=abs(r)-abs(l);
				tmp.sign=-1;
				return tmp;
			}
		}
	} else {
		if (r.sign>0) {
				tmp=abs(r)+abs(l);
				tmp.sign=1;
				return tmp;
				
		} else {
				tmp=abs(r)+abs(l);
				tmp.sign=-1;
				return tmp;
		}
	}
	tmp.normalize_number();
	return tmp;
}

Long& Long::operator=(const Long& l) {
	if (this == &l)
		return *this;
	delete [] number;
	number = new digit [size=l.size];
	if (l.sign>0)
		sign=1;
	else
		sign=-1;
	for (int i=0; i<size; i++) {
		number[i]=l.number[i];
	}
	return *this;
}

void Long::normalize_number() {
	int carry;
	for (int i=0; i<size; i++) {
		if (number[i]>=BASE) {
			carry=number[i]/BASE;
			number[i+1]+=carry;
			number[i]=number[i]-carry*BASE;
		} else {
			if (number[i]<0) {
				carry=(number[i]+1)/BASE-1;
				number[i+1]+=carry;
				number[i]=number[i]-carry*BASE;
			}
		}
	}
	int offset;
	for (offset=0; number[size-1-offset]==0; offset++) {}
	size=size-offset;

}


bool operator<(const Long &a, const Long &b) {
	if (a.size!=b.size)
        return a.size<b.size;
    for (int i=a.size-1;i>=0;i--) {
        if (a.number[i]!=b.number[i])
            return a.number[i]<b.number[i];
    }
    return false;
} 

bool operator>(const Long& a, const Long& b) {
	return b<a;
}

bool operator<=(const Long& a, const Long& b) {
	return !(a > b);
}

bool operator>=(const Long& a, const Long& b) {
	return !(a < b);
}

bool operator==(const Long &a, const Long &b) {
        return !(b < a) && !(a < b);
}

bool operator!=(const Long& a, const Long& b) {
	
	return !( a == b);
}

Long operator*(const Long &r, const Long& l) {
	Long tmp;
	if ((tmp.size=r.size+l.size)>MAXSIZE) {
		OverflowSizeException overflow_exception;
		throw overflow_exception;
	}
	tmp.number= new digit [tmp.size];
	for (int i=0; i<tmp.size; i++)
	    tmp.number[i]=0;
	
	for (int i=0; i<r.size; i++)
		for (int j=0; j<l.size; j++) {
				tmp.number[i+j]+=r.number[i]*l.number[j];
			}
	if (r.sign==l.sign)
		tmp.sign=1;
	else
		tmp.sign=-1;
	tmp.normalize_number();
	return tmp;	
}

Long& operator+=(Long& a, const Long& b) {
	a=a+b;
	return a;
}

Long& operator-=(Long& a, const Long& b) {
	a=a-b;
	return a;
}

Long& operator*=(Long& a, const Long& b) {
	a=a*b;
	return a;
}

ostream& operator<<(ostream & out, const Long& l) {
	if (l.sign<0)
		out<<"-";

	for (int i=l.size-1; i>=0; i--) {
		if (i==l.size-1) {
			out<<l.number[i];
			continue;
		}
		if (length(l.number[i])<length(BASE)-1) {
			for (int j=0; j<length(BASE)-length(l.number[i])-1; j++)
				out<<"0";
		}
		out<<l.number[i];
	}
	return out;
}


istream& operator>>(istream & in, Long& l) {
	int j;
	string s;
	in>>s;
	int s_size=s.size();
	char *str=new char [s_size];
	strcpy(str, s.c_str());
	if (str[0]=='-') {
		l.sign=-1;
		str++;
		s_size--;
	}
	else {
		l.sign=1;
	}
	if (s_size%(length(BASE)-1)==0)
		l.size=s_size/(length(BASE)-1);
	else
		l.size=s_size/(length(BASE)-1)+1;

	if (l.size>MAXSIZE) {
		OverflowSizeException overflow_exception;
		throw overflow_exception;
	}
	l.number=new digit [l.size];
	for (int i=(int)strlen(str), j=0; i>0 && j<l.size; i-=4, j++) {
		str[i] = 0;
		l.number[j]= atoi (i>=4 ? str+i-4 : str);
	}
	
	return in;
}



Long& Long::operator-() {
	if (sign==-1)
		sign = 1;
	else
		sign=-1;
	return *this;	
}

void Long::extend_number() {
	int *tmp=number;
	number=new digit [size+1];
	for (int i=0; i<size; i++)	
		number[i]=tmp[i];
	size++;
	for (int j=size-1; j>0; j--)
		number[j]=number[j-1];
	number[0]=0;
}
   
Long operator/(const Long &a, const Long &b) {
	if (abs(b)==abs(0)) {
		 DivideByZeroException divide_exception;
		 throw divide_exception;
	}
	if (abs(b)>=abs(a))
		return Long(1);
	Long res;
	if (a.sign==b.sign)
		res.sign=1;
	else
		res.sign=-1;

	int length_a=0, length_b=0;
	for (int k=0; k<a.size; k++) {
		if (a.size==1) {
			length_a=length(a.number[0]);
			break;
		}
		if(k!=a.size-1)
			length_a=length_a+length(BASE)-1;
		else
			length_a+=length(a.number[k]);
    }

	for (int k=0; k<b.size; k++) {
		if (b.size==1) {
			length_b=length(b.number[0]);
			break;
		}
		if(k!=b.size-1)
			length_b=length_b+length(BASE)-1;
		else
			length_b+=length(b.number[k]);
    }
	res.size=0;
	int length_res=length_a-length_b+1;
	int length_base=length(BASE)-1;
	if (a.size!=b.size) {
		while (length_res>=0) {
			res.size++;
			length_res-=length_base;
		}
	} else {
		res.size=1;
	}
	res.number=new digit [res.size];
    Long curValue;
	curValue.sign=1;
	curValue.size=0;
    for (int i = a.size-1, j=res.size-1; i>=0; i--, j--) {
        curValue.extend_number(); 
        curValue.number[0] = a.number[i];
		while (curValue<b) {
			curValue.extend_number(); 
        	curValue.number[0] = a.number[--i];
		}
        int x = 0;
        int l = 0, r = BASE;
        while (l<=r) {
            int m = (l + r)/2;
            Long cur = b * m;
            if (cur<=curValue) {
                x = m;
                l = m+1;
            } else {
                r = m-1;
			}
        }
        res.number[j] = x;
	    curValue = curValue - b * x;
	}
     return res;
}


Long operator%(const Long& a, const Long& b) {
	return a-(a/b)*b;
}

pair <Long, Long> divmod(const Long& a, const Long& b) {
	return make_pair(a/b, a%b);
}
