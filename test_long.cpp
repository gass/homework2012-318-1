#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

#include "test_long.h"

using namespace CppUnit;

CPPUNIT_TEST_SUITE_REGISTRATION(LongArithmeticTest);

void LongArithmeticTest::setUp() {
	L1 = new Long(1);
	L2 = new Long(2);
	L3 = new Long(3);
}

void LongArithmeticTest::tearDown() {
	delete L1;
	delete L2;
	delete L3;
}

void LongArithmeticTest::testAddition() {
	*L1 = Long (999);
	*L2 = Long (1);
	*L3 = Long (1000);
	CPPUNIT_ASSERT(*L1+*L2==*L3);
	*L1 = Long ("1238400004511000154");
	*L2 = Long ("7845165154651848120");
	*L3 = Long("9083565159162848274");
	CPPUNIT_ASSERT(*L1+*L2==*L3);
}

void LongArithmeticTest::testSubtraction() {
	*L1 = Long("1000000000000");
	*L2 = Long("999999999999");
	*L3 = Long("1");
	CPPUNIT_ASSERT(*L1-*L2==*L3);
	*L1 = Long("7894123105148941818948789410000");
	*L2 = Long("1234894");
	*L3 = Long("7894123105148941818948788175106");
	CPPUNIT_ASSERT(*L1-*L2==*L3);
}

void LongArithmeticTest::testMul() {
	*L1 = Long("789");
	*L2 = Long("415");
	*L3 = Long("327435");
	CPPUNIT_ASSERT((*L1)*(*L2)==*L3);
}

void LongArithmeticTest::testLess() {
	*L1 = Long ("123840000451100");
	*L2 = Long ("111111115154651848120");
	CPPUNIT_ASSERT(*L1<*L2);
}

void LongArithmeticTest::testGreat() {
	*L1 = Long ("971548410000519");
	*L2 = Long ("111111117223948");
	CPPUNIT_ASSERT(*L1>*L2);
}

void LongArithmeticTest::testLessEqual() {
	*L1 = Long ("13159489");
	*L2 = Long ("918333458");
	CPPUNIT_ASSERT(*L1<=*L2);
	*L1 = Long ("123456789");
	*L2 = Long ("123456789");
	CPPUNIT_ASSERT(*L1<=*L2);
}

void LongArithmeticTest::testGreatEqual() {
	*L1 = Long ("97154841000051912315848948");
	*L2 = Long ("111111117223948");
	CPPUNIT_ASSERT(*L1>=*L2);
	*L1 = Long ("123456789");
	*L2 = Long ("123456789");
	CPPUNIT_ASSERT(*L1>=*L2);
}

void LongArithmeticTest::testDivision() {
	*L1 = Long ("971548410000519");
	*L2 = Long ("11111111722");
	*L3 = Long ("87439");
	CPPUNIT_ASSERT((*L1)/(*L2) == *L3);
}

void LongArithmeticTest::testEqual() {
	*L1 = Long ("1234567891011121314");
	*L2 = Long ("1234567891011121314");
	CPPUNIT_ASSERT(*L1 == *L2);
}

void LongArithmeticTest::testNotEqual() {
	*L1 = Long ("1234567891011221314");
	*L2 = Long ("1234567891011121314");
	CPPUNIT_ASSERT(*L1 != *L2);
}

void LongArithmeticTest::testMod() {
	*L1 = Long ("12345");
	*L2 = Long ("123");
	*L3 = Long ("45");
	CPPUNIT_ASSERT(*L1%*L2 == *L3);
}

int main( int argc, char **argv) {
    CppUnit::TextUi::TestRunner runner;
    CppUnit::TestFactoryRegistry&  registry=CppUnit::TestFactoryRegistry::getRegistry();
    runner.addTest( registry.makeTest() );
    runner.run();
    return 0;
}


