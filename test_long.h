﻿#ifndef TEST_LONG_H
#define TEST_LONG_H

#include <cppunit/extensions/HelperMacros.h>
#include "Long.h"

using namespace CppUnit;
/**
 * Данный класс описывает набор тестов для объектов класса
 * Long. Класс наследутся от предописаного класса TestFixture из 
 * библиотеки CppUnit.
 * <p>В класс LongArithmeticTest включены основные проверки арифметических операций.
*/
class LongArithmeticTest : public TestFixture {
	CPPUNIT_TEST_SUITE(LongArithmeticTest);
	CPPUNIT_TEST(testAddition);
	CPPUNIT_TEST(testSubtraction);
	CPPUNIT_TEST(testMul);
	CPPUNIT_TEST(testDivision);
	CPPUNIT_TEST(testEqual);
	CPPUNIT_TEST(testMod);
	CPPUNIT_TEST(testNotEqual);
	CPPUNIT_TEST(testLess);
	CPPUNIT_TEST(testGreat);
	CPPUNIT_TEST(testLessEqual);
	CPPUNIT_TEST(testGreatEqual);
	CPPUNIT_TEST_SUITE_END();

public:
/**
 * Данный метод выделяет память для данных объекта класса LongArithmeticTest.
*/
	void setUp();
/**
 * Данный метод очищает память для данных объекта класса LongArithmeticTest.
*/
	void tearDown();	
/**
 * Тест на сложение.
*/
	void testAddition();
/**
 * Тест на вычитание.
*/
	void testSubtraction();
/**
 * Тест на умножение.
*/
	void testMul();
/**
 * Тест на равенство.
*/
	void testEqual();
/**
 * Тест на не равенство.
*/
	void testNotEqual();
/**
 * Тест на деление.
*/
	void testDivision();
/**
 * Тест на остаток от деления.
*/
	void testMod();
/**
 * Тест на меньше.
*/
	void testLess();
/**
 * Тест на больше.
*/
	void testGreat();
/**
 * Тест на меньше или равно.
*/
	void testLessEqual();
/**
 * Тест на больше или равно.
*/
	void testGreatEqual();
private:
	Long *L1, *L2, *L3;
};


#endif //TEST_LONG_H
