#include <iostream>

#include "Long.h"
#include "exception.h"

using namespace std;

int main(int argc, char **argv)
{
	try
	{
    Long a, b, c;
    cin >> a >> b;
    c = 3;
    c += a / b + a % b;
    cout << - a + b + c + 5 << endl;
	}
	catch (GeneralException & e) { e. what(); }
	catch(char * str) {cout<<str;}
	catch (...) { cout<< "Unknown error."<<endl; }
    return 0;
}

