﻿#ifndef EXCEPTION_H
#define EXCEPTION_H

#include <iostream>
/**
 * Данный абстрактный класс реализует общее исключение.
 * Функция <tt>what()</tt> является чисто виртуальной.
 * @author Антон Гой
*/
class GeneralException {
public:
    virtual void what()=0;
};
/**
 * Класс-исключений деления на ноль.
 * Этот класс является классом наследником класса GeneralException.
 * @author Антон Гой
*/
class DivideByZeroException : public GeneralException {
public:
/**
 * Данный метод печатет в поток вывода сообщение, что произошло 
 * деление на ноль. Метод необходимо вызывать в соответствующем
 * catch - обработчике.
*/
	void what();
};
/**
 * Класс-исключений превышения величины константы <i>MAXSIZE</i>.
 * Этот класс является классом наследником класса GeneralException.
 * @author Антон Гой
*/
class OverflowSizeException : public GeneralException {
public:
/**
 * Данный метод печатет в поток вывода сообщение, что произошло 
 * превышение величины <i>MAXSIZE</i>. Метод необходимо вызывать в соответствующем
 * catch - обработчике.
*/
	void what();
};

#endif // EXCEPTION_H
